/**
 * Authentication gof passport services
 */

// Modules 3rd Party
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt  = require('jsonwebtoken');

// Modules Local
var config = require('./config');

// Modules Local Mongoose Models
var User = require('./models/user');    // Document to be referenced with user login details


exports.local = passport.use(new LocalStrategy(User.authenticate() ));

// Takes care of sessions support in passport
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// creates token
exports.getToken = function(user) {
  return jwt.sign(user, config.secretKey, 
    {expiresIn: 3600});
};

var opts = {};  // jwt strategy options
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();   // specify how extraction of token is to happen
opts.secretOrKey = config.secretKey;

exports.jwtPassport = passport.use( new JwtStrategy(opts, 
  (jwt_payload, done) => {
    console.log("JWT payload: ", jwt_payload);

    User.findOne({ _id: jwt_payload._id}, (err, user) => {
      if (err) {
        return done( err, false );
      }
      else if( user ) {
        return done(null, user);
      }
      else {
        return done(null, false);
      }
    });
  }
));

exports.verifyUser = passport.authenticate('jwt', {session: false});  // Verifying incominig user


/**
 * Check if user has admin privileges
 *  This can be mounted to the various routes (dishRouters.js index.js ...etc) where admin rights is required
 */
exports.verifyAdmin = (req, res, next) => {
  // Checking db
  // req.user._id -> user param comes from exports.jwtPassport passthrough
  console.log(`Checking user _id: ${req.user._id}`);
  User.findOne({ _id: req.user._id})
  .then( (user) => {

    if(user.admin) {
      next();
    }
    else {
      err = new Error("User not authorized to perform operation. Admin privilege required!");
      err.status = 403;
      return next(err);
    }

  }, (err) => next(err))
  .catch( (err) => next(err));  
}

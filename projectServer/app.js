
// Modules 3rd Party
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
  var FileStore = require('session-file-store')(session);   // takes session as parameter
var passport = require('passport');
var authenticate = require('./authenticate');


// Modules Local
var dishRouter   = require('./routes/dishRouter');
var leaderRouter = require('./routes/leaderRouter');
var promoRouter  = require('./routes/promoRouter');
var indexRouter  = require('./routes/index');
var usersRouter  = require('./routes/usersRouter');
var uploadRouter = require('./routes/uploadRouter');
var config = require('./config');

const mongoose = require('mongoose');
const Dishes = require('./models/dishes');

const url = config.mongoUrl;
const connect = mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});

connect.then((db) => {
    console.log("Connected database correctly to server");
}, (err) => { console.log(err); });


var app = express();

  // directing ALL http reqs (port: 3000) to https (port: 3443)
  app.all('*', (req, res, next) =>{
    if(req.secure) { // or -> req.protocol == 'https'
      return next();
    }
    else{
      // app.get -> configured in www file to port 3443
      console.log('test');
      console.log(`https://${req.hostname}:${app.get('secPort')}${req.url}`);

      // Status code: 307 -> respresents target resource resides temp under diferent url
      res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
    }
  }) 

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use(passport.initialize());   


app.use('/', indexRouter);
app.use('/users', usersRouter);

  // Authentication Section

  //  
  /////////////////////////

app.use(express.static(path.join(__dirname, 'public')));    // Allows serving of static data from public folder

app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promoRouter);

app.use('/imageUpload', uploadRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

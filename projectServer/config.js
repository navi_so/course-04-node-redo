/**
 * config info for server
 */

 module.exports = {
   'secretKey': 'secret-key',
   'mongoUrl': 'mongodb://localhost:27017/conFusion'
 }
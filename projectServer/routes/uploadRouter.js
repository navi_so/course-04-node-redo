
// Modules 3rd Partu
const express = require('express');
const bodyParser = require('body-parser');
//const mongoose = require('mongoose');
const multer = require('multer');

// Modules Local
const authenticate = require('../authenticate');
const cors = require('./cors');  

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images');
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const imageFileFilter = (req, file, cb) => {
  if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return cb(new Error("Image files to be uploaded only! "));
  }
  cb(null, true);
};

const upload = multer({
  storage: storage,
  fileFilter: imageFileFilter
});

const uploadRouter = express.Router();
uploadRouter.use(bodyParser.json());

uploadRouter.route('/')
/**
 *  Preflight request, client first sends https options requires mesage and then obtain 
 *    reply from server sife before it actually sends the actual request
 */
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200})  

.get(cors.cors, authenticate.verifyUser, authenticate.verifyAdmin, (req,res, next) => {
  res.statusCode = 403;
  res.end('GET operation not supported on /imageUpload');
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /imageUpload');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /imageUpload');
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, 
  // upload -> configured settings above 
  upload.single('imageFile'), (req, res, next) => {
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(req.file);   // object has path  
  }
)



module.exports = uploadRouter;
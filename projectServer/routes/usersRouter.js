
// Modules 3rd Party
var express = require('express');
var passport = require('passport');

// Modules Core
var bodyParser = require('body-parser');

// Modules Local
const User = require('../models/user');
var authenticate = require('../authenticate');
const cors = require('./cors');  

var userRouter = express.Router();

userRouter.use(bodyParser.json());

/* GET users listing. of all database users that have signed up */
userRouter.get('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, function(req, res, next) {
  
  User.find( {} )
  .then((result) => {
    ;
    console.log(result);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  }, (err) => next(err))

  // res.send('respond with a resource');
});

userRouter.post('/signup', cors.corsWithOptions, (req, res, next) => {
  // check if username already exists
  User.register(new User({username: req.body.username}), 
    req.body.password, (err, user) => {
      if(err){
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        res.json({err: err});
      }
      else {
        if(req.body.firstname){
          user.firstname = req.body.firstname;
        }
        if(req.body.lastname){
          user.lastname = req.body.lastname;
        }

        user.save( (err, user) => {
          if(err) {
            res.statusCode = 500;
            res.setHeader('Content-Type', 'application/json');
            res.json({err: err});
            return ;
          }
          passport.authenticate('local')(req, res, () => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: true, status: 'Registration Successful'});
          });
        });

      }
  });
});


userRouter.post('/login', cors.corsWithOptions, passport.authenticate('local') , (req, res, next) => { 

  var token = authenticate.getToken({ _id: req.user._id });
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({
    success: true, 
    token: token,
    status: 'You are successfully logged in!'
  });
});

userRouter.get('/logout', cors.corsWithOptions, (req, res, next) => {
  if(req.session) {
    req.session.destroy();          // deleting session on server side
    res.clearCookie('session-id');  // delete cookie on client side
    res.redirect('/');
  }
  else {
    var err = new Error('You are not logged in!');
    next(err);
  }
});

module.exports = userRouter;

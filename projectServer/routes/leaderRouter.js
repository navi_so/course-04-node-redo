
// Modules 3rd party
const express = require('express');
const bodyParser = require('body-parser');

// Modules Local
const Leaders = require('../models/leaders.js');
const authenticate = require('../authenticate');
const cors = require('./cors');   

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
/**
 *  Preflight request, client first sends https options requires mesage and then obtain 
 *    reply from server sife before it actually sends the actual request
 */
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200})  

.get(cors.cors, (req, res, next) => {
  Leaders.find({})
  .then( (leaders) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(leaders);
  }, (err) => next(err))
  .catch( (err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Leaders.create( req.body )
  .then( (leaders) => {
    console.log(`Leader ${req.body.name} created`);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(leaders);
  }, (err) => next(err)
  )
  .catch( (err) => next(err)); 
  //res.end('Will add the leader: ' + req.body.name + '\nwith details: ' + req.body.description);
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Leaders.remove({})
  .then( (resp) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(resp);
  }, (err) => next(err)
  )
  .catch( (err) => next(err) );
});


leaderRouter.route('/:leaderId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200})  

.get(cors.cors, (req, res, next) => {
  Leaders.findById(req.params.leaderId)
  .then( (leader) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(leader);
  }, (err) => next(err)
  )
  .catch( (err) => next(err) );
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST ops not supported');
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Leaders.findByIdAndUpdate(
    req.params.leaderId,
    { $set: req.body },
    { new: true}
  )
  .then( (leader) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(leader);
  }, (err) => next(err)
  )
  .catch( (err) => next(err) );
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Leaders.findByIdAndDelete( req.body.leaderId)
  .then( (resp) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(resp);
  }, (err) => next(err)
  )
  .catch( (err) => next(err) );
});

module.exports = leaderRouter;
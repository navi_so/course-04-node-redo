/**
 * 
 */

// Modules 3rd Party
const cors = require('cors');     // Cross Origin Resource Sharing
const express = require('express');


const app = express();

const whiteList = ['http://localhost:3000', 'https://localhost:3443'];  // Whitelist of all origins cors accepts

var corsOptionsDelegate = (req, callback) => {
  var corsOptions;

  if( whiteList.indexOf(req.header('Origin')) !== -1 ) {  // missing array values from indexOf returns -1
    corsOptions = { origin: true };
  }
  else {
    corsOptions = { origin: false };
  }
  callback(null, corsOptions);

};

exports.cors = cors();    // empty params allow access controle allow Orogin with wild cards *
exports.corsWithOptions = cors(corsOptionsDelegate);
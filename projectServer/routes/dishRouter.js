
// Modules 3rd Partu
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


// Modules Local
const Dishes = require('../models/dishes');
const authenticate = require('../authenticate');
const cors = require('./cors');     

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());

dishRouter.route('/')
/**
 *  Preflight request, client first sends https options requires mesage and then obtain 
 *    reply from server sife before it actually sends the actual request
 */
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200})  

.get(cors.cors, (req,res,next) => {
  Dishes.find({})

  // Mongoose population support
  .populate('comments.author')  // Ensure author field is also populated

  .then((dishes) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dishes);
  }, (err) => next(err))
  .catch((err) => next(err));
})

/**
 * We now authenticating users -> (authenticate.verifyUser)
 *  pass: proceed with next functino (req, res, next) => {}
 *  fail: reply with err msg via passport auth
 */
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Dishes.create(req.body)
  .then((dish) => {
      console.log('Dish Created ', dish);
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dish);
  }, (err) => next(err))
  .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /dishes');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Dishes.remove({})
  .then((resp) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(resp);
  }, (err) => next(err))
  .catch((err) => next(err));    
});


dishRouter.route('/:dishId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200}) 

.get(cors.cors, (req,res,next) => {
  Dishes.findById(req.params.dishId)

  // Mongoose population support
  .populate('comments.author')  // Ensure author field is also populated

  .then((dish) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dish);
  }, (err) => next(err))
  .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /dishes/'+ req.params.dishId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Dishes.findByIdAndUpdate(req.params.dishId, {
      $set: req.body
  }, { new: true })
  .then((dish) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dish);
  }, (err) => next(err))
  .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Dishes.findByIdAndRemove(req.params.dishId)
  .then((resp) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(resp);
  }, (err) => next(err))
  .catch((err) => next(err));
});


/**
 * Sub-document modifications -> comments section REST API endpoints
 */

dishRouter.route('/:dishId/comments')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200}) 

.get(cors.cors, (req,res,next) => {
  Dishes.findById( req.params.dishId)

  // Mongoose population support
  .populate('comments.author')  // Ensure author field is also populated

  .then((dish) => {
    if(dish != null) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dish.comments);
    }
    else {
      err = new Error('Dish ' + req.params.dishId + ' not found');
      err.stat = 404;
      return next(err);
    }
  }, (err) => next(err))
  .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  Dishes.findById(req.params.dishId)
  .then((dish) => {
    if(dish != null) {

      req.body.author = req.user._id;   // authenticate.verify user already loaded the user in req.obj

      dish.comments.push(req.body);
      dish.save()
      .then( (dish) => {

        // Mongoose population mod
        Dishes.findById(dish._id)
          .populate('comments.author')
          .then( (dish) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(dish);
          })
      }, 
      (err) => next(err) ); 
    }
    else {
      err = new Error('Dish ' + req.params.dishId + ' not found');
      err.stat = 404;
      return next(err);
    }

  }, (err) => next(err))
  .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /dishes ' + req.params.dishId + '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Dishes.findById(req.params.dishId)
  .then((dish) => {
      if( dish != null) {
        // Not alternative in removing subdocuments, must remove one by one
        for ( var i = (dish.comments.length - 1); i >= 0; i--) {
          dish.comments.id(dish.comments[i]._id).remove();
        }
        dish.save()
        .then( (dish) => {
          res.statusCode=200;
          res.setHeader('Content-Type', 'application/json');
          res.json(dish);
        }, (err) => next(err)
        );
      }
  }, (err) => next(err))
  .catch((err) => next(err));    
});

dishRouter.route('/:dishId/comments/:commentId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200}) 

.get(cors.cors, (req,res,next) => {
  Dishes.findById(req.params.dishId)

  // Mongoose population support
  .populate('comments.author')  // Ensure author field is also populated

  .then((dish) => {

    if( (dish != null) && dish.comments.id(req.params.commentId) != null) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(dish.comments.id(req.params.commentId));
    }
    else if (dish == null) {
      err = new Error('Dish ' + req.params.dishId + ' not found');
      err.stat = 404;
      return next(err);
    }
    
    else {
      err = new Error('Comment ' + req.params.commentId + ' not found');
      err.stat = 404;
      return next(err);
    }

  }, (err) => next(err))
  .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /dishes/'+ req.params.dishId + '/comments/' + req.params.commentId);
})

.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  Dishes.findById(req.params.dishId)
  .then((dish) => {
      if (dish != null && dish.comments.id(req.params.commentId) != null) {
        console.log(`incomining user id: ${req.user._id}`);
        console.log(`Database auth id:   ${dish.comments.id(req.params.commentId).author}`);
        
        // Assign 03 Task 04  - Make sure users can only modify their own comments 
          if (req.user._id.equals(dish.comments.id(req.params.commentId).author) ){
            console.log("USER IS CORRECT");

          }
          else {
            console.log("USER IS !CORRECT");
            err = new Error("User cannot modify another users comments!");
            err.status = 404;
            return next(err);
          }
        //
        //////////////////////////////
          if (req.body.rating) {
              dish.comments.id(req.params.commentId).rating = req.body.rating;
          }
          if (req.body.comment) {
              dish.comments.id(req.params.commentId).comment = req.body.comment;                
          }
          dish.save()
          .then((dish) => {

            // Mongoose population mod
            Dishes.findById( dish._id )
            .populate('comments.author')
            .then( (dish) => {
              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.json(dish);     
            })
          
          }, (err) => next(err));
      }
      else if (dish == null) {
          err = new Error('Dish ' + req.params.dishId + ' not found');
          err.status = 404;
          return next(err);
      }
      else {
          err = new Error('Comment ' + req.params.commentId + ' not found');
          err.status = 404;
          return next(err);            
      }
  }, (err) => next(err))
  .catch((err) => next(err));
})

.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  Dishes.findById(req.params.dishId)
  .then((dish) => {

    if (dish != null && dish.comments.id(req.params.commentId) != null) {
      console.log(`incomining user id: ${req.user._id}`);
      console.log(`Database auth id:   ${dish.comments.id(req.params.commentId).author}`);
      
      // Assign 03 Task 04  - Make sure users can only delete their own comments 
        if (req.user._id.equals(dish.comments.id(req.params.commentId).author) ){
          console.log("USER IS CORRECT");
          dish.comments.id(req.params.commentId).remove();
        }
        else {
          err = new Error("User cannot delete another users comments!");
          err.status = 404;
          return next(err);
        }

      //
      //////////////////////////////

        
      //dish.comments.id(req.params.commentId).remove();
      dish.save()
      .then((dish) => {

        // Mongoose population mod
        Dishes.findById( dish._id )
        .populate('comments.author')
        .then( (dish) => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(dish);     
        })               

      }, (err) => next(err));
    }
    else if (dish == null) {
      err = new Error('Dish ' + req.params.dishId + ' not found');
      err.status = 404;
      return next(err);
    }
    else {
      err = new Error('Comment ' + req.params.commentId + ' not found');
      err.status = 404;
      return next(err);            
    }
  }, (err) => next(err))
  .catch((err) => next(err));
});



module.exports = dishRouter;
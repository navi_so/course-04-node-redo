// Modules 3rd Party
const express = require('express');
const bodyParser = require('body-parser');

// Modules Local
const Promotions = require('../models/promotions');
const authenticate = require('../authenticate');
const cors = require('./cors');   

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());

promoRouter.route('/')
/**
 *  Preflight request, client first sends https options requires mesage and then obtain 
 *    reply from server sife before it actually sends the actual request
 */
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200})  

.get(cors.cors, (req, res, next) => {
  Promotions.find({})
  .then( (promos) => {
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(promos);
    }, 
    (err) => next(err) 
  )
  .catch( (err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  //console.log(req.body);
  Promotions.create(req.body)
  .then( (promos) => {
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(promos);
  }, (err) => next(err)
  )
  .catch(  (err) => next(err));

})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Promotions.deleteMany({})
  .then( (resp) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(resp);
    //console.log(resp);
  }, (err) => next(err)
  )
  .catch( (err)=>next(err));
  //res.end('Deleting all promos');
});


promoRouter.route('/:promoId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus = 200}) 

.get(cors.cors, (req, res, next) => {
  Promotions.findById(req.params.promoId)
  .then( (promo) => {
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(promo);
  }, (err) => next(err)
  )
  .catch( (err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST ops not supported');
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  console.log(req.body);
  Promotions.findByIdAndUpdate( 
    req.params.promoId, 
    { $set: req.body },
    { new: true }
  )
  .then( (promo) =>{
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(promo);
  }, (err) => next(err))
  .catch( (err) => next(err));
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
  Promotions.findByIdAndDelete(req.params.promoId)
  .then( (resp) => {
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(resp);
  }, (err) => next(err)
  )
  .catch( (err) => next(err));
});

module.exports = promoRouter;
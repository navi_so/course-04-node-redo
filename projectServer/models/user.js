const mongoose = require('mongoose');

/**
 * const mongoose = require('mongoose');
    const Schema = mongoose.Schema;
  is the same as below in one line
 */
const Schema = require('mongoose').Schema;
var passportLocalMongoose = require('passport-local-mongoose');

const userSchema = new Schema({
  firstname: {
    type: String,
    default: ''
  },
  lastname: {
    type: String,
    default: ''   
  },

  // NOTE: passport-local-mongoose already provides username + password plugin
  admin: {
      type: Boolean,
      default: false      
  }
}, { timestamps: true }
);


userSchema.plugin(passportLocalMongoose);

const Users = mongoose.model('User', userSchema);

module.exports = Users;

// Or -> module.exports = mongoose('User', userSchema);
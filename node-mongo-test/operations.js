/**
 * File based node module
 * Encapsulates the 4 main database operations -> insert, find, update and remove
 */

const assert = require('assert');

/**
 * Parameters:
 *  db - mongodb datbase connection
 *  document - document to insert
 *  collection - collection into which you want to insert to document 
 *  callback (Depracated) - callback once ops is completed 
 */
exports.insertDocument = (db, document, collection, callback) => {
  const coll = db.collection(collection);
  return coll.insert(document);
};

/**
 * Parameters:
 *  db - mongodb datbase connection
 *  collection - collection into which you want to insert to document 
 *  callback (Depracated) - callback once ops is completed
 */
exports.findDocuments = (db, collection, callback) => {
  const coll = db.collection(collection);
  return coll.find({}).toArray();
};

/**
 * Parameters:
 *  db - mongodb datbase connection
 *  document - document to insert
 *  collection - collection into which you want to insert to document 
 *  callback (Depracated) - callback once ops is completed
 */
exports.removeDocument = (db, document, collection, callback) => {
  const coll = db.collection(collection);
  return coll.deleteOne(document);
};

/**
 * Parameters:
 *  db - mongodb datbase connection
 *  document - document to insert
 *  collection - collection into which you want to insert to document 
 *  callback (Depracated) - callback once ops is completed
 * 
 *  update - field to replace current data with update
 */
exports.updateDocument = (db, document, update, collection, callback) => {
  const coll = db.collection(collection);
  return coll.updateOne(document, { $set: update }, null);
};
// Modules 3rd Party
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Modules Local
const dboper = require('./operations');

const url = 'mongodb://localhost:27017/';
const dbname = 'conFusion';

MongoClient.connect(url, { useUnifiedTopology: true} ).then( (client) => {

  console.log('\nAfter Insert: \n');
  //console.log(result.ops);

  const db = client.db(dbname);
  
  dboper.insertDocument(db, {name: "Vadonut", description: 'Test'}, 'dishes')
  .then( (result) => {
    console.log('Insert Document:\n', result.ops );

    return dboper.findDocuments(db, 'dishes')
  })
  .then( (docs) => {
    console.log('Found Documents:\n', docs);

    return dboper.updateDocument(db, {name: 'Vadonut'}, {description: 'Updated test 1'}, 'dishes');
  })
  .then( (result) => {
    console.log('Updated Doc:\n', result.result);

    return dboper.findDocuments(db, 'dishes');
  })
  .then( (docs) => {
    console.log('Found Documents:\n', docs);

    return db.dropCollection('dishes');
  })
  .then( (result) => {
    console.log('Dropped Collection: ', result);
    client.close();
  })
  .catch((err) => console.log(err));
})
.catch((err) => console.log(err));